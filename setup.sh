#!/bin/bash


function check {
  if [ -d $1 ]; then
    echo "$1 already exists --- aborting"
    exit 1
  fi
}

function install {
  git clone "git@gitlab.com:kaku.ink/$1.git" -b develop
  pushd $1
  npm install
  popd
}

function link {
  pushd $2
  npm link ../$1
  popd
}


check common
check frontend
check overworld
check tile-queuer
check tile-renderer
check tile-server

install common
install frontend
install overworld
install tile-queuer
install tile-renderer
install tile-server

link common frontend
link common overworld
link common tile-queuer
link common tile-renderer
link common tile-server

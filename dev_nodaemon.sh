#!/bin/bash

BASEDIR=$(dirname $(readlink -f "$0"))
DEFAULT_NPM_PROGRESS=$(npm get progress)

function run {
  pushd "$BASEDIR/$1" > /dev/null
  npm set progress=false
  npm run $2
  npm set progress=$DEFAULT_NPM_PROGRESS
  popd > /dev/null
}

run common build

run frontend build &
run overworld build
run tile-queuer build
run tile-renderer build
run tile-server build

run overworld start &
run tile-queuer start &
run tile-server start &

run tile-renderer start &
run tile-renderer start &
run tile-renderer start &
run tile-renderer start

#!/bin/bash


BASEDIR=$(dirname $(readlink -f "$0"))
IP=$(ip route get 1 | awk '{print $NF;exit}')


export NODE_ENV="development"
export DEBUG="kaku:*"

export KAKU_REDIS_ADDRESS="127.0.0.1"
export KAKU_REDIS_PORT="6379"
unset KAKU_REDIS_PASSWORD

export KAKU_OVERWORLD_ADDRESS="0.0.0.0"
export KAKU_OVERWORLD_PORT="8080"

export KAKU_TILE_SERVER_ADDRESS="0.0.0.0"
export KAKU_TILE_SERVER_PORT="8081"

export KAKU_FRONTEND_OVERWORLD_URL="http://$IP:$KAKU_OVERWORLD_PORT/"
export KAKU_FRONTEND_TILE_BASE="http://$IP:$KAKU_TILE_SERVER_PORT/"

export KAKU_DATA_DIR="$BASEDIR/tmp/data/"
mkdir -p $KAKU_DATA_DIR


nodemon \
  -e "scss,pug,js" \
  -w "$BASEDIR/common/src" \
  -w "$BASEDIR/frontend/src" \
  -w "$BASEDIR/overworld/src" \
  -w "$BASEDIR/tile-queuer/src" \
  -w "$BASEDIR/tile-renderer/src" \
  -w "$BASEDIR/tile-server/src" \
  -x "$BASEDIR/dev_nodaemon.sh"
